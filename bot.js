'use strict';
var http = require('http');
var url = require('url');
var spawn = require('child_process').spawn;
var crypto = require("crypto");
var fs = require('fs');

var URL;
if (!URL) {
    URL = require('url').URL;
}
console.log(URL);

var TelegramClient = require('telegram-basic-bot');
var token = '';

var telegram = new TelegramClient(token);

telegram.poll(['message']);

telegram.on('update', (update) => {
	console.log(update);
    console.log(update.message.entities);
    if (update.message.entities) {
        update.message.entities.forEach((entity) => {
            if (entity.type === 'url') {
                var url = update.message.text.substring(entity.offset, entity.offset + entity.length);
                processUrl(update, url);
            }
        });
    }
});

function processUrl(update, urlString) {
    if (!urlString.endsWith('webm')) {
        console.log('not webm' + urlString);
        return;
    }
    var url = new URL(urlString);
    if (!(url.hostname.endsWith('4chan.org') ||
        url.hostname.endsWith('4cdn.org'))) {
        console.log('not supported ' + urlString);
        return;
    }
    console.log('processing ' + urlString);
    var filename = makeId() + '.mp4';
    console.log('outputting ' + filename);

    var ffmpeg = spawn('ffmpeg',
        ['-i', 'pipe:',
         '-vcodec', 'libx264',
         '-preset', 'fast',
         '-profile:v', 'main',
         '-an',
         '/var/www/html/webmfier/' + filename]);
    ffmpeg.stdout.on('data', (data) => { console.log('' + data); });
    ffmpeg.stderr.on('data', (data) => { console.log('' + data); });
    ffmpeg.on('close', (code, signal) => {
        console.log('finished ffmpeg ' + code + '   ' + signal);
        console.log('filename ' + filename);
        telegram.request('sendAnimation',
            { chat_id: update.message.chat.id, animation: 'http://chiwa.girlish.moe/webmfier/' + filename },
            (err, res) => { console.log(err); console.log(res); });

        setTimeout(() => {
            fs.unlink('/var/www/html/webmfier/' + filename);
        }, 600000);
    });
    ffmpeg.stdin.on('error', (err) => {
        console.log('spawn error' + err);
    });

    var fetchOptions = {
        port: 80,
        method: 'GET',
        hostname: url.hostname,
        path: url.pathname,
    };
    var request = http.request(fetchOptions, (res) => {
        res.pipe(ffmpeg.stdin);
        console.log(res.headers);
        res.on('error', (err) => {
            console.log('res error' + err);
        });
    });
    request.end();
    request.on('error', (err) => {
        console.log(err);
        ffmpeg.stdin.end();
    });
}

telegram.on('error', (err) => {
    console.log(err);
});

function makeId() {
    return crypto.randomBytes(16).toString("hex");
}
